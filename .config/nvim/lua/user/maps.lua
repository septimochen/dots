local keymap = vim.keymap

keymap.set('n', 'x', '"_x')

-- Increment/decrement
keymap.set('n', '+', '<C-a>')
keymap.set('n', '-', '<C-x>')

-- Delete a word backwards
keymap.set('n', 'dw', 'vb"_d')

-- Select all
keymap.set('n', '<C-a>', 'gg<S-v>G')

-- Save with root permission (not working for now)
--vim.api.nvim_create_user_command('W', 'w !sudo tee > /dev/null %', {})

-- New tab
keymap.set('n', 'te', ':tabedit')
-- Split window
keymap.set('n', 'ss', ':split<Return><C-w>w')
keymap.set('n', 'sv', ':vsplit<Return><C-w>w')
-- Move window
keymap.set('n', '<Space>', '<C-w>w')
keymap.set('', 'sh', '<C-w>h')
keymap.set('', 'sk', '<C-w>k')
keymap.set('', 'sj', '<C-w>j')
keymap.set('', 'sl', '<C-w>l')

-- Resize window
keymap.set('n', '<C-w><left>', '<C-w><')
keymap.set('n', '<C-w><right>', '<C-w>>')
keymap.set('n', '<C-w><up>', '<C-w>+')
keymap.set('n', '<C-w><down>', '<C-w>-')

keymap.set('n', '<C-n>', ":NERDTreeToggle<CR>")

-- Move lines
keymap.set('n', '<C-k>', ':m .-2<CR>==')
keymap.set('n', '<C-j>', ':m .+1<CR>==')
keymap.set('i', '<C-j>', '<Esc>:m .+1<CR>==gi')
keymap.set('i', '<C-k>', '<Esc>:m .-2<CR>==gi')
keymap.set('v', '<C-j>', ':m \'>+1<CR>gv=gv')
keymap.set('v', '<C-k>', ':m \'<-2<CR>gv=gv')

-- Toggle relative number
keymap.set('n', 'snr', ':set norelativenumber<CR>')
keymap.set('n', 'srn', ':set relativenumber<CR>')