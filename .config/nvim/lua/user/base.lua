vim.cmd("autocmd!")

vim.scriptencoding = 'utf-8'
vim.opt.encoding = 'utf-8'
vim.opt.fileencoding = 'utf-8'

-- vim.wo.number = true

vim.opt.autoindent = true
vim.opt.autowrite = true
vim.opt.backspace = { 'start', 'eol', 'indent' }
vim.opt.backup = false
vim.opt.backupskip = { '/tmp/*', '/private/tmp/*' }
vim.opt.breakindent = true
vim.opt.cmdheight = 3
vim.opt.foldenable = false
vim.opt.hidden = true
vim.opt.hlsearch = true
vim.opt.ignorecase = true -- Case insensitive searching UNLESS /C or capital in search
vim.opt.inccommand = 'split'
vim.opt.laststatus = 2
vim.opt.lazyredraw = true
vim.opt.mouse = ''
vim.opt.path:append { '**' } -- Finding files - Search down into subfolders
vim.opt.relativenumber = true
vim.opt.scrolloff = 10
vim.opt.showcmd = true
vim.opt.smartindent = true
vim.opt.smarttab = true
vim.opt.swapfile = false
vim.opt.title = true
vim.opt.ttyfast = true
vim.opt.updatetime = 300
vim.opt.wildignore:append { '*/node_modules/*' }
vim.opt.wrap = false -- No Wrap lines

-- Undercurl
vim.cmd([[let &t_Cs = "\e[4:3m"]])
vim.cmd([[let &t_Ce = "\e[4:0m"]])

-- Use wide tabs
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4
vim.opt.expandtab = true

-- Turn off paste mode when leaving insert
vim.api.nvim_create_autocmd("InsertLeave", {
  pattern = '*',
  command = "set nopaste"
})

-- Add asterisks in block comments
vim.opt.formatoptions:append { 'r' }

local filetypes = { 'html', 'js', 'yaml', 'yml', 'lua' }
for _, filetype in ipairs(filetypes) do
  vim.api.nvim_create_autocmd("FileType", {
    pattern = filetype,
    command = "setlocal shiftwidth=2 tabstop=2"
  })
end
